import {
    Component,
    OnInit
} from '@angular/core';
import { select } from '@angular-redux/store';
import { Observable } from 'rxjs/Observable';

import { CounterActionCreator } from '../../store/counter';

@Component({
    selector: 'home-page',
    styleUrls: ['./home.page.scss'],
    templateUrl: './home.page.html'
})
export class HomePage implements OnInit {
    @select(['counter', 'value']) counter$: Observable<number>;

    constructor(
        private counterActions: CounterActionCreator
    ) {}

    public ngOnInit() {
        console.log('HomePage initialized');
    }

    public incrementCounter(factor: number): void {
        this.counterActions.increment(factor);
    }

    public decrementCounter(factor: number): void {
        this.counterActions.decrement(factor);
    }

}
