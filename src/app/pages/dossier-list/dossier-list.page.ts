import {
    Component,
    OnInit
} from '@angular/core';
import { select } from '@angular-redux/store';
import { Observable } from 'rxjs/Observable';

import { Dossier, DossierList, DossierListActionCreator } from '../../store/dossier-list';
import { DossierService } from '../../services'

@Component({
    selector: 'dossier-list-page',
    styleUrls: ['dossier-list.page.scss'],
    templateUrl: './dossier-list.page.html'
})
export class DossierListPage implements OnInit {
    @select(['dossierList']) dossiers$: Observable<DossierList>;
    public loading = true;
    public newDossier = new Dossier();

    constructor(
        private dossierListActions: DossierListActionCreator,
        private dossierService: DossierService
    ) {}

    public ngOnInit() {
        this.dossierService.getDossiers().subscribe(dossiers => {
            this.dossierListActions.addDossiers(dossiers);
            this.loading = false;
            console.log(this.dossiers$);
        });
    }

    public createDossier() {
        this.dossierListActions.addDossier(this.newDossier);
        this.newDossier = new Dossier();
    }
}
