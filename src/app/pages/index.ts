import { HomePage } from './home';
import { NoContentPage } from './no-content';
import { DossierListPage } from './dossier-list';

export const Pages = [
    DossierListPage,
    HomePage,
    NoContentPage
];
