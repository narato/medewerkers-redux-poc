import { Injectable } from '@angular/core';
import { NgRedux } from '@angular-redux/store';

import { AppState } from '../index';
import * as actionTypes from './dossier-list.action-types';
import { Dossier, DossierList, DossierListAction } from './dossier-list.types';

@Injectable()
export class DossierListActionCreator {
    constructor(
        private ngRedux: NgRedux<AppState>
    ) {}

    public addDossiers(dossiers: DossierList): void {
        this.ngRedux.dispatch({
            type: actionTypes.ADD_DOSSIERS,
            payload: dossiers
        });
    }

    public addDossier(dossier: Dossier): void {
        this.ngRedux.dispatch({
            type: actionTypes.ADD_DOSSIER,
            payload: dossier
        });
    }
}
