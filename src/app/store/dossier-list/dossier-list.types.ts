export class Dossier {
  id: number;
  dossierNr: string;
  subject: string;
  dateOfApplication: Date;
  applicant: string;
};

export type DossierList = Array<Dossier>;

export type DossierListAction = {
    type: string;
    payload: DossierList;
}
