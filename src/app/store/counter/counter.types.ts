export type Counter = {
    previous: number;
    value: number;
};

export type CounterAction = {
    type: string;
    factor: number;
}
