import { Injectable } from '@angular/core';
import { NgRedux } from '@angular-redux/store';

import { AppState } from '../index';
import * as actionTypes from './counter.action-types';
import { CounterAction } from './counter.types';

@Injectable()
export class CounterActionCreator {
    constructor(
        private ngRedux: NgRedux<AppState>
    ) {}

    public increment(factor: number): void {
        setTimeout(() => {
            this.ngRedux.dispatch(this.updateCounter(factor, true));
        }, 10);
    }

    public decrement(factor: number): void {
        setTimeout(() => {
            this.ngRedux.dispatch(this.updateCounter(factor, false));
        }, 10);
    }

    private updateCounter(factor: number, shouldIncrement: boolean): CounterAction {
        return {
            type: shouldIncrement ? actionTypes.COUNTER_INCREMENT : actionTypes.COUNTER_DECREMENT,
            factor
        };
    }
}
