import { Counter, CounterAction } from './counter.types';
import { COUNTER_INITIAL_STATE } from './counter.initial-state';
import * as actionTypes from './counter.action-types';

export const counterReducer = (
    state: Counter = COUNTER_INITIAL_STATE,
    action: CounterAction
) => {
    switch (action.type) {
        case actionTypes.COUNTER_INCREMENT:
            return {
                previous: state.value,
                value: state.value + action.factor
            };
        case actionTypes.COUNTER_DECREMENT:
            return {
                previous: state.value,
                value: state.value - action.factor
            };
        default:
            return state;
    }
};
