import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

import { Dossier } from '../store/dossier-list/dossier-list.types';
import { LoggerService } from './logger.service';

@Injectable()
export class DossierService {
  static dossiersUrl = 'http://localhost:3030/dossiers';

  constructor(private http: Http, private logger: LoggerService) {}

  getDossiers(): Observable<Dossier[]> {
    return this.http.get(DossierService.dossiersUrl).map(response => response.json()).catch(this.logger.handleError);
  }
}
