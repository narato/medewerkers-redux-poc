import { NgModule, ApplicationRef } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgRedux, NgReduxModule, DevToolsExtension } from '@angular-redux/store';

import { AppComponent } from './app.component';
import { Pages } from './pages';
import { Components } from './components';
import { AuiModules } from './aui.modules';
import { LoggerService, DossierService } from './services';

import { AppRoutingModule } from './app-routing.module';

import { initialState, StoreModule } from './store';
import { AppState, rootReducer, storeMiddleware } from './store';

const DEVMODE = process.env.NODE_ENV === 'DEV';


@NgModule({
    imports: [
        BrowserModule,
        HttpModule,
        FormsModule,
        ReactiveFormsModule,
        NgReduxModule,
        StoreModule,
        AppRoutingModule,
        ...AuiModules
    ],
    declarations: [
        AppComponent,
        ...Pages,
        ...Components
    ],
    providers: [
        LoggerService,
        DossierService
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
    constructor(
        public appRef: ApplicationRef,
        private ngRedux: NgRedux<AppState>,
        private devTools: DevToolsExtension
    ) {
        const enhancers = DEVMODE && devTools.isEnabled() ? [ devTools.enhancer() ] : [];

        this.ngRedux.configureStore(rootReducer, initialState, [
            ...storeMiddleware
        ], enhancers);
    }
}
