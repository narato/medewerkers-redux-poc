import { GreeterComponent } from './greeter';
import { CounterComponent } from './counter';

export const Components = [
    GreeterComponent,
    CounterComponent
];
