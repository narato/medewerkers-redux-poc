import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';

@Component({
    selector: 'counter',
    templateUrl: './counter.component.html',
    styleUrls: ['./counter.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class CounterComponent {
    @Input() value: number;
    @Output() increment: EventEmitter<number> = new EventEmitter();
    @Output() decrement: EventEmitter<number> = new EventEmitter();

    public factor = 0;

    incrementCounter() {
        this.increment.emit(this.factor);
    }

    decrementCounter() {
        this.decrement.emit(this.factor);
    }
}
