const proxy = require('http-proxy-middleware');
const history = require('connect-history-api-fallback');
const SERVER_CONFIG = require('./package.json').config;

const ENV = process.env.NODE_ENV || 'DEV';
const apiProxyMiddleware = proxy('/api', {
    target: `http://localhost:${SERVER_CONFIG.api.port}`,
    pathRewrite: {
        '^/api': ''
    }
});
const fallbackMiddleware = history({
    index: '/index.html',
    verbose: true
});

module.exports = {
    "port": SERVER_CONFIG.app.port,
    "server": {
        'baseDir': ENV === 'TEST' ? './coverage' : './dist',
        'middleware': ENV === 'TEST' ? {} : {
            1: fallbackMiddleware,
            2: apiProxyMiddleware
        }
    }
};
