module.exports = {
    link: [
        /** <link> tags for a Web App Manifest **/
        { rel: 'manifest', href: '/manifest.json' },
        { rel: 'stylesheet', href: 'http://cdn-z.antwerpen.be/core_branding_scss/1.0.0-beta.1/main.min.css' }
    ],
    meta: []
};
