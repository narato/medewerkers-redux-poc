module.exports = {
    title: 'Medewerkersreduxpoc app | ACPaaS Angular App generator',
    description: 'Medewerkersreduxpoc app made with the ACPaaS Angular App generator.',
    baseUrl: '/',
    themeColor: '#CE1A32',
    GTMContainerId: '',
    GATag: ''
};
